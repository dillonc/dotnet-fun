using DotnetFun.Enums;

namespace DotnetFun.Entities;

public class GuitarSpec
{
    private Builder Builder { get; set; }
    private string Model { get; set; }
    private GuitarType Type { get; set; }
    private Wood BackWood { get; set; }
    private Wood TopWood { get; set; }
    private int NumStrings { get; set; }
    
    public Builder GetBuilder()
    {
        return Builder;
    }
    
    public string GetModel()
    {
        return Model;
    }
    
    public GuitarType GetGuitarType()
    {
        return Type;
    }
    
    public Wood GetBackWood()
    {
        return BackWood;
    }
    
    public Wood GetTopWood()
    {
        return TopWood;
    }
    
    public int GetNumStrings()
    {
        return NumStrings;
    }

    public GuitarSpec(Builder builder, string model, GuitarType type, int numStrings, Wood backWood, Wood topWood)
    {
        Builder = builder;
        Model = model;
        Type = type;
        NumStrings = numStrings;
        BackWood = backWood;
        TopWood = topWood;
    }

    public bool Matches(GuitarSpec otherSpec)
    {
        if (Builder != otherSpec.GetBuilder())
        {
            Console.WriteLine("Builder not found.");
            return false;
        }
            
        string model = otherSpec.Model.ToLower();
        // if ((model != null) && (!model.Equals("")) && (!Model.ToLower().Equals(model)))
        if ((model != null) && (!model.Equals("")) && (!Model.ToLower().Equals(model)))
        {
            Console.WriteLine("Model not found.");
            return false;
        }
            
        if (Type != otherSpec.Type)
        {
            Console.WriteLine("Type not found.");
            return false;
        }
        
        if (NumStrings != otherSpec.NumStrings)
        {
            Console.WriteLine("NumStrings not found.");
            return false;
        }
            
        if (BackWood != otherSpec.BackWood)
        {
            Console.WriteLine("Backwood not found.");
            return false;
        }
            
        if (TopWood != otherSpec.TopWood)
        {
            Console.WriteLine("Topwood not found.");
            return false;
        }

        return true;
    }
}
