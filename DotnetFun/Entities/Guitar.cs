using DotnetFun.Enums;

namespace DotnetFun.Entities;

public class Guitar : GuitarSpec
{
    private string SerialNumber { get; set; }
    private double Price { get; set; }
    private GuitarSpec Spec { get; set; }

    public Guitar(string serialNumber, double price, GuitarSpec guitarSpec) : base(guitarSpec.GetBuilder(), guitarSpec.GetModel(), guitarSpec.GetGuitarType(), guitarSpec.GetNumStrings(), guitarSpec.GetBackWood(), guitarSpec.GetTopWood())
    {
        SerialNumber = serialNumber;
        Price = price;
        Spec = guitarSpec;
    }
    
    public string GetSerialNumber()
    {
        return SerialNumber;
    }
    
    public double GetPrice()
    {
        return Price;
    }

    public GuitarSpec GetSpec()
    {
        return Spec;
    }
}
