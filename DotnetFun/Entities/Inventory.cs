using DotnetFun.Enums;

namespace DotnetFun.Entities;

public class Inventory
{
    private List<Guitar> guitars;
    
    public Inventory()
    {
        guitars = new List<Guitar>();
    }
    
    public void AddGuitar(string serialNumber, double price, Builder builder, string model, GuitarType type, int numStrings, Wood backWood, Wood topWood)
    {
        Guitar guitar = new Guitar(serialNumber, price, new GuitarSpec(builder, model, type, numStrings, backWood, topWood));
        guitars.Add(guitar);
    }
    
    public Guitar? GetGuitar(string serialNumber)
    {
        foreach (Guitar guitar in guitars)
        {
            if (guitar.GetSerialNumber().Equals(serialNumber))
            {
                return guitar;
            }
        }
        return null;
    }
    
    public List<Guitar> Search(GuitarSpec searchSpec)
    {
        List<Guitar> guitarsFound = new List<Guitar>();
        foreach (Guitar guitar in guitars)
        {
            if (guitar.Matches(searchSpec))
            {
                guitarsFound.Add(guitar);
            }
        }
        return guitarsFound;
    }
}
