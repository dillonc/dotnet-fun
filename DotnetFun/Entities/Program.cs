﻿using DotnetFun.Enums;

namespace DotnetFun.Entities;

internal class Program
{
    public static void Main(string[] args)
    {
        Inventory inventory = new Inventory();
        InitializeInventory(inventory);

        // var searchItem = new Guitar("11277", 3_999.95, Builder.COLLINGS, "CJ", GuitarType.ACCOUSTIC, Wood.INDIAN_ROSEWOOD, Wood.SITKA);
        var whatErinLikes = new GuitarSpec(Builder.FENDER, "Stratocastor", GuitarType.ELECTRIC, 6, Wood.ALDER, Wood.ALDER);
        // var whatErinLikesTwelveString = new GuitarSpec(Builder.FENDER, "Stratocastor", GuitarType.ELECTRIC, 12, Wood.ALDER, Wood.ALDER);
        var guitars = inventory.Search(whatErinLikes);
        
        if (guitars.Any())
        {
            guitars.ForEach(guitar =>
            {
                GuitarSpec guitarSpec = guitar.GetSpec();
                Console.WriteLine(
                    $"You might like this {guitarSpec.GetBuilder()} {guitarSpec.GetModel()} {guitarSpec.GetNumStrings()}-string {guitarSpec.GetGuitarType()} guitar:\n  {guitarSpec.GetBackWood()} back and sides,\n  {guitarSpec.GetTopWood()} top.\nYou can have it for only ${guitar.GetPrice()}!");
            });
        }
        else
        {
            Console.WriteLine("Sorry, we have nothing for you.");
        }
    }

    public static void InitializeInventory(Inventory inventory)
    {
        inventory.AddGuitar("11277", 3_999.95, Builder.COLLINGS, "CJ", GuitarType.ACCOUSTIC, 6, Wood.INDIAN_ROSEWOOD, Wood.SITKA);
        inventory.AddGuitar("V95693", 1_499.95, Builder.FENDER, "Stratocastor", GuitarType.ELECTRIC, 6, Wood.ALDER, Wood.ALDER);
        inventory.AddGuitar("V9512", 1_549.95, Builder.FENDER, "Stratocastor", GuitarType.ELECTRIC, 6, Wood.ALDER, Wood.ALDER);
    }
}
