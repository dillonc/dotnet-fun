namespace DotnetFun.Enums;

public enum Builder
{
    FENDER,
    MARTIN,
    GIBSON,
    COLLINGS,
    OLSON,
    RYAN,
    PRS,
    ANY
}
